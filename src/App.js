import './App.css';
import Input from './Input';
import TodoList from './TodoList';

function App() {
  return (
    <div className='container'>
     <TodoList></TodoList> 
    </div>
  );
}

export default App;
