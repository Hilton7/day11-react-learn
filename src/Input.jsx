import "./Input.css"
export default function ({ array, setArray }) {
    function addItem() {
        let inputItem = document.getElementsByTagName("input")[0]
        let val = inputItem.value
        if (val == '' || val.trim() == '') {
            alert("不能添加空数据哦")
            return
        }
            
        inputItem.value = ''
        setArray([...array, val])
    }
    return (
        <div className="input-box">
            <input type="text" /> <button className="button" onClick={addItem}>add</button>
        </div>
    )
}