import { useState } from 'react'
import Input from './Input'
import ListItem from './ListItem'
import './TodoList.css'
export default function () {
    let [array, setArrary] = useState(new Array())
    
    return (
        <div className='todo-list-box'>
            <h4>To do List</h4>
            <ListItem array={array}></ListItem>
            <Input array={array} setArray={setArrary}></Input> 
        </div>
    )
}