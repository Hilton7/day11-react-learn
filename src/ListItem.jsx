import './ListItem.css'
export default function ({array}) {
    return (
        <div className="list-box">
            {array.map(val => {
                return (
                    <div className="list-box-item">
                       {val}
                    </div>
                )
            })}
        </div>
    )
}