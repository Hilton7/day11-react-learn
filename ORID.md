# **What did we learn today? What activites did you do? What scenes have impressed you**
Today I learned about React, and did exercises related to React and CSS and property selectors, of which the practice of counter sums was the most profound to me.
# **Pleas use one word to express your feelings about today's class.**
useful.
# **What do you think about this? What was the most meaningful aspect of this activity?**
What you learned today is very useful, and React learning is the most useful of them.
# **Where do you most want to apply what you have learned today? What changes will you make?**
I most wanted to use React knowledge in future development, and React made me change my thinking about front-end development.